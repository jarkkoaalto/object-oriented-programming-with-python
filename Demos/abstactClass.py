from abc import ABC, abstractmethod

class Institute(ABC):
    def __init__(self):
        print(type(self).__name__, "details :")

    def coursesOffered(self):
        print("Courses Offered : C, C++ , Java, .Net")

    @abstractmethod
    def address(self):pass

class TechnoAcademy(Institute):
    def coursesOffered(self):
        print("Courses offered: python data science, Xamarin")

    def address(self):
        print("Address @ Hyderabad")

class OnlineAcademy(Institute):
    def address(self):
        print("Address @ Bangalore")


ta = TechnoAcademy()
ta.coursesOffered()
ta.address()

olt = OnlineAcademy()
olt.coursesOffered()
olt.address()
#inst = Institute()
