class GrandParents:
    def PropertyLand(self):
        print("Property Land for Farming given by GrandParents")

class Parents(GrandParents):
    def PropertyHome(self):
        print("Property honme constructed by Parents")

class Child(Parents):
    def PropertyVehicle(self):
        print("Property Car Purchased by Child")

c1 = Child()
c1.PropertyLand()
c1.PropertyHome()
c1.PropertyVehicle()