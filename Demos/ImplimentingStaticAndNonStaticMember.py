class Employee:

    TotalEmployees = 0
    def __init__(self, empno, ename, salary, deptno):
        self.EmployeeNo = empno
        self.Ename = ename
        self.Salary = salary
        self.DepartmentNo = deptno
        Employee.TotalEmployees += 1

    def showEmployee(self):
        print("Employee #: {} \nEmployee Name: {} \nSalary: {} \nDepatment # : {}".format(self.EmployeeNo, self.Ename, self.Salary, self.DepartmentNo))

emp1 = Employee(101,"Kim",4500, 10)
emp2 = Employee(102,"Kia",4400, 10)
emp3 = Employee(103, "Herman",4550, 5)

print("Total Employees: ", Employee.TotalEmployees)
emp1.showEmployee()
emp2.showEmployee()
emp3.showEmployee()