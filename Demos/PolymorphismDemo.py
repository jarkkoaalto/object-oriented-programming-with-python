class  English:
    def greet(self, name):
        print("Good morning", name)

class French:
    def greet(self, name):
        print("Bonjour", name)

def greetings(language, name):
    language.greet(name)

english = English()
french = French()

greetings(english, "Ukko")
greetings(french, "Ukko")