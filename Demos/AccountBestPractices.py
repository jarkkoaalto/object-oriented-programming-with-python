class Test:
    staticVariable = 0
    instanceVariable = 0


    def __init__(self):
        print("Constructing the Object for Test Class")
        self.instanceVariable += 1
        Test.staticVariable += 1

t1 = Test()
print("After creating first object:")
print("Instance Variable:", t1.instanceVariable)
print("Static variable :", t1.staticVariable)

t2 = Test()
print("After creating second object :")
print("Instance Variable: " , t2.instanceVariable)
print("Static Variable : ", t2.staticVariable)
print("Static Variable Using class ref. : ", Test.staticVariable)