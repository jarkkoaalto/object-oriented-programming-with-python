class Product:
    def __init__(self):
        self.__ProductID = ""
        self.__ProductName = ""
        self.__Price = ""

    def setProduct(self, pid, pname, price):
        self.__ProductID = pid
        self.__ProductName  = pname
        self.__Price = price

    def updateProduct(self, newPrice):
        self.__Price = newPrice

    def showProduct(self):
        print("Product ID : {} \n Product Name: {} \n Price: {}".format(self.__ProductID, self.__ProductName, self.__Price))


tv = Product()
tv.setProduct("TV101", "LG Golden Eye", 18500)
#tv.__ProductName = "Hilton Golden Eye" 
#tv.__Price = 7000
tv.showProduct()
tv.updateProduct(6000)
tv.showProduct()