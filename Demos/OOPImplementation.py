from abc import ABC, abstractmethod

class Account(ABC):
    def __init__(self, accontNo, customerName, accountType, balance):
        self.AccountNo = accontNo
        self.CustomerName = customerName
        self.AccountType = accountType
        self.Balance = balance


    def __str__(self):
        return "Account No. : {} \nCustomer Name : {}\nAccount Type : {}\nBalance : {} \n".format(self.AccountNo, self.CustomerName, self.AccountType, self.Balance)

    def deposit(self,amount):
        self.Balance += amount

    @abstractmethod
    def withDraw(self, amount):pass

class SavingAccount(Account):
    def withDraw(self, amount):
        if(self.Balance - amount < 500):
            raise Exception("Balance Amount is lesser than RS. 500")
        else:
            self.Balance -= amount

class CurrentAccount(Account):
    def withDraw(self, amount):
        self.Balance -= amount

sa = SavingAccount(101, "John", "SAVING", 15000)
print(sa)

amount = float(input("Enter Amount to be withdrawn : "))
sa.withDraw(amount)
print("Current Balance : ", sa.Balance)


la = SavingAccount(102, "Laura", "SAVING", 150000)
print(la)
amount = float(input("Enter Amount to be withdrawn : "))
la.withDraw(amount)
print("Current Balance : ", la.Balance)