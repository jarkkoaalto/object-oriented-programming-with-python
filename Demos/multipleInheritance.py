class Father:
    def FatherProperty(self):
        print("Father's Property")


class Mother:
    def MotherProperty(self):
        print("Mother Property")

class Child(Father, Mother):
    def Property(self):
        print("Child will inherit:")
        super().FatherProperty()
        super().MotherProperty()

c1 = Child()
c1.Property()