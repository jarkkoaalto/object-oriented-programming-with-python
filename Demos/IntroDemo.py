class Name:
    def __init__(self, firstName, middleName, lastName):
        self.FirstName = firstName
        self.MiddleName = middleName
        self.LastName = lastName

class Student:
    def __init__(self, rollNo, sname, course):
        self.RollNo = rollNo
        self.StudentName = sname
        self.Course = course

student1 = Student(101, Name("John","Matias","Doe"),"Object oriented programming with Python")
student2 = Student(102, Name("Jenny","Mia","Deeds"),"Object oriented programming with Python")
student3 = Student(103, Name("Laura","Jenny","Martin"),"Object oriented programming with Python")

students = [student1,student2, student3]

for student in students:
    print("Roll Number: {} \n Student Name: {} {} {} \n Course: {}".format(student.RollNo, student.StudentName.FirstName, student.StudentName.MiddleName, student.StudentName.LastName, student.Course))