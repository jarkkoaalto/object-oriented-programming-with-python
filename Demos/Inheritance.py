class Employee:

    def __init__(self, empno, ename, salary, deptno):
        self.EmployeeNo = empno
        self.Ename = ename
        self.Salary = salary
        self.DepartmentNo = deptno
        

    def showEmployee(self):
        print("Employee #: {} \nEmployee Name: {} \nSalary: {} \nDepatment # : {}".format(self.EmployeeNo, self.Ename, self.Salary, self.DepartmentNo))

class Salesman(Employee):
    def __init__(self,empno, ename, salary, deptno, comm):
        self.Commission = comm 
        super().__init__(empno, ename, salary, deptno)

emp = Salesman(101,"Shrek", 4000, 10, 400)
emp.showEmployee()

print("Commission: ", emp.Commission)