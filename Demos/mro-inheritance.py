class X: pass

class Y: pass

class Z: pass

class A(X,Y): pass
class B(Y,Z): pass

class N(A,B,Z): pass

for obj in N.mro():
    print(obj)


# $ python mro-inheritance.py
# <class '__main__.N'>
# <class '__main__.A'>
# <class '__main__.X'>
# <class '__main__.B'>
# <class '__main__.Y'>
# <class '__main__.Z'>
# <class 'object'>