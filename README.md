# Object Oriented Programming with Python #

## What is class ? ##

A Class is used to define an user defined datatype and can be considered as a blue-print for maintaining the data 

Syntax:

class ClassName: # Specify the member(s) of the class 

## What is object? ##

Instance of class is said to be an object

Object stored the data for the specified typed defined using the class. Object are isolated.

Syntax:

objectName = className([argumentValue(s)])

Classes are the blue print of the Prototypes

Objects are the instance fo the class which stores the data physically

## What are constructor? ##

Constructors are special methods which are used to initalise the member of the clas whenever we create an instrce.

Systax:

```python
def __init__(self,[argumentValue(s)]):
    self.MemeberName = argumentValue
    ...

```

```python
class Point:
    def __init__(self,x,y):
        self.X = x
        self.Y = y

startPoint = Point(0,0)
```

## Understanding static and non-static members ##

### Non static members

Whenever a member is bound with the objects defined for the class then it is said to be Instance Member or Non-Static Members.

 By default the members of the class are Instance Members


### Static member

Whenever a member is bound with the Class Definition then it is said to be Static Members

To access:

ClassName.MemberName

or 
ObjectName.MemberName

Values of the Static Member is Shared across all the Objects defined for the Class

### When to use static and instance merbers of a class?

Whenever the value has to be shared then define that member as a static member.

Whenever the value has to be specific for every individual object define the member as an instance member


|    SchoolStudent      |                       |
| --------------------- | --------------------- |
| RegistrationID        |   Non-Static members  |
| StudentName           |                       |
| CourseName            |                       |
| Fees                  |                       |
| JoinDate              |                       |
| --------------------- | --------------------- |
| Total Student         | Static Member         |



| SchoolPythonStudent   |                       |
| --------------------- | --------------------- |
| Registraiton ID       | Non-Static Member     |
| StudentName           | Non-Static Member     |
| CourseName            | Static Member         |
| Fees                  | Static Member         |
| JoinDate              | Non-Static Member     |
| TotalStudents         | Static Member         |


## What is Data Abstration ?

Data abstraction is a Process of Removing Physical, Spatial or Temporal Details or Attributes in the Study of Objects or Systems in order to focus attention on details of higher importance.

Data abstration is a process to identify the relevant and irrelevant data for the user.

### What is Data emcapsulation ? 

Data encapsulation is a process of building of data with the methods that operate on the data.

Data encapsulation is a process of restricting the direct access fo the data members.

## Implementing Data abstraction & data encapsulation ##

Identifying the requirements i.e. What is the information to be provided and what is the information to be hidden from the user that logical identification is Data abstraction.

To implementing tha  practically we use data encapsulation by allowing the user to access the data by exposing methods is data encapsulation.

|       Product         |
| --------------------- |
| ProductID             |
| ProductName           |
| Price                 |


WRONG: p = Product() p.ProductName = "..."

p.updatePrice(newPrice)

## Understanding Ingeritance ##

### What is inheritance ###
It is feature which is used to inherit the existing class definition by another class such that the members of the parent class can be accessed from the child class.


Base class


| ParentClass                   |
| ----------------------------- |
|   # Feature of parent's class |

Derived Class

| ChildClass                    |
| ----------------------------- |
| # Feature of parent's class   |
| And freatures of child class  |


## Method overriding ##
Whenever the definitions of the Base Class is modified at the Derived Class then it is said to be Method Overriding.

No additional keyword to special syntax is required.

## Multiple inheritance ##

Whenever a class inherits from more than one Base class then it is said to be Multiple Inheritance

|   BaseClass 1         |       BaseClass 2      |
| --------------------- | ---------------------- |
| Feature of baseclass1 |  Feature of baseclass1 |

|           MultiDerivedClass                      |
| ------------------------------------------------ |
| Class DerivedClass (BaseClass1, BaseClass2)[...] |
|                                                  |
| # Features of BaseClass 1                        |
| # Features of BaseClass 2                        |
| # Features of MultiDerivedClass                  |

## Multi-Level Inheritance

Whenever a class ingerits from other Derived Class then it is said to be Multi-Level Inheritance.

## MRO inheritance ##

In Multiple Inheritance Scenario, any specified attribute or variable or method is searched first in the Current Class. If not found then the search continues into Parent Class in depth-first, then left-right fashion without searching the same class twice. This order is called as Linearisation of Multi-Derived class and the set of rules used to find this order is called as Method Resolution Order (MRO).

MRO must prevent the local precedence ordering and also provide monotonicity.

## Understanding Abstract Class ##

### What is abstract Class? ###

In simple terms an abstract Class is a class which consists of the methods With Definitions and without Definitions.

## Understanding how to Convert Traditional Python Code to OOP Implementation ##
- Define the Parameter used within the nested-if statement 
- The Case expression or the valuees userd for the condition should be converted as a Derived class
- The Action statements or Expressions of the if-statement or switch case will be definition for the method present at the Derived Class. 
- If the switch case consists of a default expression in other languages then that definition will become the virtual method at the Base Class.
- If the default Expression is not present in switch case then the definition will become the abstract method at the Base Class.
- If any Common Variables are used crossed all the switch expressions or conditions then that variables will become the variables or field at the Base Class.

## Understanding polymorphism

Whenever an entity can change its behaviour based on the arguments supplied to the entity then it is said to be polymorphism.

- Method overloading
- Method Overriding
- Polymorphism with Functions
- Operator Overloading

## Polymorphism with Functions

Polymorphism with functions is an ability on Object Oriented Programming to use a common interface for multiple form data types.

## Operator Overloading ##

Python Operators work for build-in classes and the same operator behaves differetly with different datatypes